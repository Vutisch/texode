<?php
namespace DB;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaTool;
use Entity\Comment;

class  Connect{

    public  function __construct()
    {
        require_once 'vendor/autoload.php';
        require_once 'Entity/Comment.php';

        $paths = array('Entity');

        $config = Setup::createAnnotationMetadataConfiguration($paths,true);

        $dpParams = array(
            'driver'=>'pdo_mysql'  ,
            'user' => 'root',
            'password'=>'13101993',
            'dbname'=>'Texode');

         $this->entityManager = EntityManager::create($dpParams,$config);
         $this->schemaTool = new SchemaTool($this->entityManager);

    }

    public  function addComment($name,$email,$comment,$image){
        $message = new  Comment();
        $message->setName($name);
        $message->setEmail($email);
        $message->setMessage($comment);
        $message->setImage($image);
        $message->setData();
        $this->entityManager->persist($message);
        $this->entityManager->flush();

    }

    public function updateSchema(){
        $this->schemaTool->dropDatabase();
        $this->schemaTool->createSchema(array($this->entityManager->getClassMetadata('Entity\Comment')));

    }

    /**
     * @var Comment $message
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getComments()
    {
        return $this->entityManager->getRepository('Entity\Comment')->findAll();
    }
}
