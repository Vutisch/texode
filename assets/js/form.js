    $(document).ready(function () {
        var files;
        $('input[type=file]').change(function(){
            files = this.files;
            var ext = this.value.match(/\.(.+)$/)[1];
            switch (ext) {
                case 'jpg':
                case 'png':
                case 'gif':
                    break;
                default:
                    alert('Разрешенные фаорматы: JPG, GIF, PNG');
                    this.value = '';
            }
        });
        $('#add_message').on( 'click', function( event ){
            event.stopPropagation();
            event.preventDefault();
            var name =  $('#message_name').val();
            var email =  $('#message_email').val();
            var message =  $('#contact_message').val();
            var data = new FormData();
            var fail ='';
            if (name.length >50 || name.length <1)
                fail = "Логин не может быть пустым и быть больше 50 символов";
            else if (email.split ('@').length - 1 == 0 || email.split ('.').length - 1 == 0)
                fail = "Некорректный email";
            else if (message.length >200 || message.length <1)
                fail = "Сообщение  не может быть пустым и не может превышать 200 символов";
            if (fail !== ''){
                alert(fail);
                return false;
            }
            $.each( files, function( key, value ){
                data.append( key, value );
            });
            data.append('name',name);
            data.append('email',email);
            data.append('message',message);

            $.ajax({
                url         : 'commentHandler.php',
                type        : 'POST',
                data        : data,
                cache       : false,
                processData : false,
                contentType : false,
                success     : function( respond, status, jqXHR ){
                },
                error: function( jqXHR, status, errorThrown ){
                    console.log( 'ОШИБКА AJAX запроса: ' + status, jqXHR );
                }

            }).done(function() {
                console.log('success');
            }).fail(function() {
                console.log('fail'+fail);
            });
        });
    });