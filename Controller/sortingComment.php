<?php

namespace Controller;

class sortingComment{


    public function sort($comments,$typeSort,$order)
    {
        $sortArr = array();
        foreach($comments as $key=>$val) {
            switch ($typeSort) {

                case 'sortByDate':
                    $sort = $val->getData();
                    $sort=$sort->getTimestamp();
                    break;
                case 'sortByName':
                    $sort = $val->getName();
                    break;
                case 'sortByEmail':
                    $sort = $val->getEmail();
                    break;
                default;
            }
            $sortArr[$key] = $sort;
        }
        array_multisort($sortArr, $order, $comments);

        return $comments;
    }
}
