<?php
/**
 * Created by PhpStorm.
 * User: vutisch
 * Date: 14.11.18
 * Time: 20:52
 */

namespace Controller;

use SplFileInfo;

class uploadImages
{
    const DIR = './assets/images/';

    private $name;
    private $tmp;

    public function __construct($name,$tmp)
    {
        $this->name =$name;
        $this->tmp =$tmp;
        $info = new SplFileInfo($this->name);
        $this->info =$info->getExtension();
    }

    /**
     * @return string
     */
    public function uploadImages()
    {
        $fileName = sprintf('%s.%s', md5(uniqid()), $this->info);
        move_uploaded_file($this->tmp, self::DIR.$fileName);
        $this->thumbnail($fileName);
        return $fileName;
    }

    public function thumbnail($fileName)
    {
        $this->createThumbnail($fileName,100,'./assets/images/admin_image/');
        $this->createThumbnail($fileName,400,'./assets/images/comment_image/');
    }


    public function createThumbnail($fileName,$width,$dir)
    {
        switch ($this->info) {
            case 'jpg':
                $im = imagecreatefromjpeg(self::DIR .  $fileName);
                break;
            case 'gif';
                $im = imagecreatefromgif(self::DIR  .$fileName);
                break;
            case 'png':
                $im = imagecreatefrompng(self::DIR  . $fileName);
                break;
            default:
                echo "Не подходязий формат";
        }

        $ox = imagesx($im);
        $oy = imagesy($im);
        $nx = $width;
        $ny = floor($oy * ($width / $ox));
        $nm = imagecreatetruecolor($nx, $ny);
        imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);

        if(!file_exists($dir)) {
            if(!mkdir($dir)) {
                die("Возникли проблемы! попробуйте снова!");
            }
        }

        imagejpeg($nm, $dir.$fileName);

    }

}