<?php

namespace Entity;

/**
 * @author  Vutisch
 * @Entity
 * @Table(name="user")
 */

class user
{
    /** @Id @Column(type="integer",unique=true) @GeneratedValue */
    private $id;

    /**
     * @Column(name="name",unique=true, type="string",length=50)
     */
    private $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @Column(name="password", type="string",length=120)
     */
    private $password;
}