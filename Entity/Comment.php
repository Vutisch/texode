<?php
/**
 * Created by PhpStorm.
 * User: vutisch
 * Date: 19.10.18
 * Time: 20:42
 */

namespace Entity;

/**
 * @author  Vutisch
 * @Entity
 * @Table(name="message")
 */
class Comment
{
    /** @Id @Column(type="integer",unique=true) @GeneratedValue */
    private $id;

    /**
     * @Column(name="name", type="string",length=50)
     */
    private $name;

    /**
     * @Column(name="email", type="string",length=120)
     */
    private $email;

    /**
     * @Column(name="message", type="string",length=200)
     */
    private $message;

    /**
     * @Column(name="image", type="string", length=100, nullable=true)
     */
    private $image;

    /**
     * @Column(type="datetime", nullable=true)
     */
    private $data;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param $image
     */
    public function setData(): void
    {
        $this->data = new \DateTime();
    }
}